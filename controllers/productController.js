const Product = require('../models/Product')
const User = require('../models/User')



//Add Products
module.exports.addProduct = (reqBody) => {
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });

    return Product.findOne({name: newProduct.name}).then(result => {
		if(result !== null) {
			return "Product is already registered"
		} else {
		    return newProduct.save().then((result, error) => {
		        if (error) {
		            return "Product registration failed"
		        } else {
		            return "Product registration successful"
		        }
		    })
		 }
    })
}



// Retrieve All Active Product
module.exports.getAllActiveProducts = () => {

    return Product.find({isActive: true}).then(result => {

        return result
    })
}


// Retrieve Specific Product by Id
module.exports.getSpecificProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then(result => {

        return result
    })

}

// Update Specific Product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, err) => {
		if(err) {
			return "Update product failed"
		} else {
			return "Update product successful"
		}
	})
}



// Archive Product
module.exports.archiveProduct = (reqParams) => {
	let updatedProduct = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, err) => {
		if(err) {
			return "Archive product failed"
		} else {
			return "Archive product successful"
		};
	});
};



