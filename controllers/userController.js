const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')



// Register User
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return User.findOne({email: newUser.email}).then(result => {
		if(result !== null) {
			return "User is already registered"
		} else {
			return newUser.save().then((user, error) => {
		        if (error) {
		            return "User registration failed"
		        } else {
		            return "User registration successful"
		        }
		    })
		 }
    })
}



// Authentication
module.exports.login = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return "User not found"

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}

	})
}

// User Detail
module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {

		result.password = "";

		return result;

	});

};


// Set User To Admin (Admin only)
module.exports.userAdmin = (reqParams) => {

	return User.findByIdAndUpdate(reqParams.userId).then((user, err) => {
		if(err) {
			return err
		} else {
			if(user) {
				return "You are now admin"
			} else {
				return "You are not admin"
			}
		};
	});
};




// User Order



