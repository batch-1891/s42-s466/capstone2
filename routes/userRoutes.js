const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const orderController = require('../controllers/orderController')
const auth = require("../auth")


// Register User
router.post("/registerUser", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Authentication
router.post("/login", (req, res) => {
	userController.login(req.body).then(resultFromController => res.send(resultFromController))
});

// User Detail
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile(userData).then(resultFromController => res.send(resultFromController))

})


// Set User To Admin (Admin only)
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin


	if(isAdmin) {
		userController.userAdmin(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	};
});

// User Order
router.post("/order", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin
	const userId = auth.decode(request.headers.authorization).id

	if(isAdmin) {
		response.send('User only')
	} else {
		orderController.createOrder(request.body, userId).then(resultFromController => response.send(resultFromController))
	}
})


// Retrieve All Orders (admin only)
router.get("/getAllOrder", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin;

	if(isAdmin) {
		orderController.retrieveAllOrders().then(resultFromController => response.send(resultFromController));
	} else {
		response.send("Admin only")
	}
})


// Retrieve User Order
router.get("/myOrder", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin
	const userId = auth.decode(request.headers.authorization).id

	if(isAdmin) {
		response.send('User only')
	} else {
		orderController.createOrder(request.body, userId).then(resultFromController => response.send(resultFromController))
	}
})

















module.exports = router;