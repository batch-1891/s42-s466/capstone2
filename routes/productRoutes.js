const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')
const auth = require("../auth")


// Create Product (Admin only)
router.post("/addProduct", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
})


// Retrieve All Active Product
router.get("/", (request, response) => {

    productController.getAllActiveProducts().then(resultFromController => response.send(resultFromController))
});

// Retrieve Specific Product by Id
router.get("/:productId", (req, res) => {

    productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController))

});

// Update Specific Product
router.put("/:productId", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if(isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
})

// Archive Product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	if(isAdmin) {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
})













module.exports = router;