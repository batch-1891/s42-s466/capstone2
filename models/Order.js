const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    userId: {
        type: String,
        required: [true, "User ID is required"]
    },
    orders: [
        {
            productId: {
                type: String,
                required: [true, "Product ID is required"]
            },
            orderQuantity: {
                type: Number,
                required: [true, "Order quantity is required"]
            },
            subtotal: {
                type: Number
            }
        }
    ]
});

module.exports = mongoose.model("Order", orderSchema);
